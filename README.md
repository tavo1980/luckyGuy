# LuckyGuy
## Descripción
LuckyGuy es una aplicación que te permitirá escoger 14 números para el sorteo Kino Chile de forma lógica e inteligente. Se obtendrán una serie de patrones y estadísticas mediante criterios de análisis y comparación. La elección de combinación candidata se realizará analizando el historial completo de sorteos ganadores.

## Criterios

- ***Análisis de frecuencia de números***, probabilidad de que salga el número 1, 2, 3, …, 25.
- ***Análisis de terminaciones de números***, probabilidad de que salgan números con la misma terminación final como por ejemplo 1, 11 ,21 o 5, 15, 25 por nombrar algunos.
- ***Cantidad máxima de números consecutivos***, por ejemplo, el primer sorteo cuenta con 4 números consecutivos 1, 2, 3, 4, el segundo 6 números 5, 6, 7, 8, 9, 10, 11 y así sucesivamente.
- ***Cantidad máxima de separación entre números consecutivos***, por ejemplo, el primer sorteo cuenta con 4 separaciones consecutivas del 1 al 6, el segundo 6 separaciones 5 al 12 y así sucesivamente.
- ***Cantidad de números de uno y dos dígitos***, por ejemplo, el primer sorteo cuenta con 6 números de un digito, 1, 2, 3, 5, 6,7, y 8 números de dos dígitos 12, 13, 14, 19, 20, 23, 24, 25.
- ***Comparación con sorteo anterior***, cantidad de números que estadísticamente se repiten en sorteo anterior.
- ***Cantidad de números primos por sorteo***.
- ***Cantidad de números pares por sorteo***.
- ***Rango máximo y mínimo por sorteo***, por ejemplo, la suma de todos los números de los sorteos esta entre 180 y 240.
- ***Sorteo candidato no debe existir anteriormente***, el sorteo seleccionado debe ser único y no debe haber sido premiado, esto aumenta la probabilidad de éxito.

## Recomendación

Seleccionar sorteo candidato y jugar siempre la misma combinación, con esto se aumentará progresivamente la probabilidad de ganar algún premio.
