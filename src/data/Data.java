package data;
import java.io.*;
import useful.*;

public class Data {
	
	private BufferedReader bufferedReader;
	private FileReader fileReader;
	
	public Data() {}
	
	public BufferedReader openFile() {
		try {
			fileReader = new FileReader(Useful.PATH);
			bufferedReader = new BufferedReader(fileReader);
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Error: openFile()");
		} 
		return bufferedReader;
	}
	
	public void closeFile(BufferedReader bufferedReader) {
		try {
			bufferedReader.close();
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Error: closeFile()");
		} 		
	}
	
}//end class