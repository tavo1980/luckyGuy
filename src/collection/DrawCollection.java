package collection;
import useful.*;
import data.*;
import java.util.*;
import java.io.*;

public class DrawCollection {
	
	private Draw draw;
	private ArrayList<Draw> drawList;
	private int[] tempArray;
	private int rows;
	
	public DrawCollection() {
		drawList = new ArrayList<Draw>();
	}	
//-----------------------------------------------------------------------	
	public void createDrawList() {
		System.out.println("> Cargando base de datos\t\tOK");
		Data file = new Data();
		BufferedReader bufferedReader = file.openFile();
		try {
			String line = bufferedReader.readLine();
			while(line!=null) {
				draw = new Draw();
				String fields[] = line.split(Useful.SEPARATOR);
				draw.createDrawArray(fields);
				drawList.add(draw);
				line = bufferedReader.readLine();
			}
		} catch (Exception e) {
			System.out.println("Error: createCollection()");
		} finally {
			file.closeFile(bufferedReader);
		}
		rows = drawList.size();
		/*
		 * ver coleccion 
		 */
		//Useful.viewDrawList(drawList);
	}		
//-----------------------------------------------------------------------	
	/*
	*  posici�n 0 = frecuencia
	*  posici�n 1 = terminaci�n 2
	*  posici�n 2 = terminaci�n 3
	*  posici�n 3 = digitos
	*  posici�n 4 = previo
	*  posici�n 5 = consecutivo
	*  posici�n 6 = separaci�n
	*  posici�n 7 = m�x
	*  posici�n 8 = m�n
	*  posici�n 9 = par
	*  posici�n 10 = primo
	*/
//-----------------------------------------------------------------------	
	public void getFrecuency(int values[]) {
		System.out.println("> Calculando frecuencia\t\t\tOK");
		tempArray = new int[rows];
		for(int i=Useful.ZERO;i<drawList.size();i++) {
			tempArray[i] = Useful.countArray(drawList.get(i).getDrawArray()); 
		}
		values[Useful.ZERO] = Useful.getNumberMoreRepeated(tempArray);
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(tempArray);	
	}
//-----------------------------------------------------------------------	
	public void getTermination(int values[]) {
		System.out.println("> Calculando terminaciones\t\tOK");
		int[] tempArrayTerminationTwo = new int[rows];
		int[] tempArrayTerminationThree = new int[rows];
		for(int i=Useful.ZERO;i<drawList.size();i++) {
			tempArrayTerminationTwo[i] = Useful.countTermination(drawList.get(i).getDrawArray(),Useful.TWO);
			tempArrayTerminationThree[i] = Useful.countTermination(drawList.get(i).getDrawArray(),Useful.THREE);
		}
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(tempArrayTerminationTwo);	
		//Useful.viewArray(tempArrayTerminationThree);	
		values[Useful.ONE] = Useful.getNumberMoreRepeated(tempArrayTerminationTwo);
		values[Useful.TWO] = Useful.getNumberMoreRepeated(tempArrayTerminationThree);
	}
//-----------------------------------------------------------------------	
	public void getDigit(int values[]) {
		System.out.println("> Calculando d�gitos\t\t\tOK");
		tempArray = new int[rows];
		for(int i=Useful.ZERO;i<drawList.size();i++) {
			tempArray[i] = Useful.countDigit(drawList.get(i).getDrawArray()); 
		}
		values[Useful.THREE] = Useful.getNumberMoreRepeated(tempArray);
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(tempArray);	
	}	
//-----------------------------------------------------------------------		
	public void getPrevius(int values[]) {
		System.out.println("> Calculando valores previos\t\tOK");
		tempArray = new int[rows];
		for(int i=Useful.ZERO;i<drawList.size()-Useful.ONE;i++) {
			tempArray[i] = Useful.comparePrevius(drawList.get(i).getDrawArray(),drawList.get(i+Useful.ONE).getDrawArray());
		}
		values[Useful.FOUR] = Useful.getNumberMoreRepeated(tempArray);
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(tempArray);
	}	
//-----------------------------------------------------------------------		
	public void getConsecutive(int values[]) {
		System.out.println("> Calculando n�meros consecutivos\tOK");
		tempArray = new int[rows];
		for(int i=Useful.ZERO;i<drawList.size();i++) {
			tempArray[i] = Useful.countConsecutives(drawList.get(i).getDrawArray());
		}
		values[Useful.FIVE] = Useful.getMaxNumberfromArray(tempArray);
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(tempArray);
	}	
//-----------------------------------------------------------------------
	public void getSeparated(int values[]) {
		System.out.println("> Calculando separaciones\t\tOK");
		tempArray = new int[rows];
		for(int i=Useful.ZERO;i<drawList.size();i++) {
			tempArray[i] = Useful.countSeparated(drawList.get(i).getDrawArray());
		}
		values[Useful.SIX] = Useful.getMaxNumberfromArray(tempArray);
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(tempArray);		
	}
//-----------------------------------------------------------------------
	public void getMaxMin(int values[]) {
		System.out.println("> Calculando rango m�x. y m�n.\t\tOK");
		tempArray = new int[rows];
		for(int i=Useful.ZERO;i<drawList.size();i++) {
			tempArray[i] = Useful.sumDraw(drawList.get(i).getDrawArray());
		}
		values[Useful.SEVEN] = Useful.getMaxNumberfromArray(tempArray);
		values[Useful.EIGHT] = Useful.getMinNumberfromArray(tempArray);
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(tempArray);			
	}	
//-----------------------------------------------------------------------	
	public void getPair(int values[]) {
		System.out.println("> Calculando n�meros pares\t\tOK");
		tempArray = new int[rows];
		for(int i=Useful.ZERO;i<drawList.size();i++) {
			tempArray[i] = Useful.countPair(drawList.get(i).getDrawArray());
		}
		values[Useful.NINE] = Useful.getNumberMoreRepeated(tempArray);
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(tempArray);				
	}	
//-----------------------------------------------------------------------	
	public void getPrime(int values[]) {
		System.out.println("> Calculando n�meros primos\t\tOK");
		tempArray = new int[rows];
		for(int i=Useful.ZERO;i<drawList.size();i++) {
			tempArray[i] = Useful.countPrime(drawList.get(i).getDrawArray());
		}
		values[Useful.TEN] = Useful.getNumberMoreRepeated(tempArray);
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(tempArray);		
	}	
//-----------------------------------------------------------------------	
	public int getNumber() {
		return drawList.get(drawList.size()-Useful.ONE).getNumber();
	}	
//-----------------------------------------------------------------------	
	public String getDate() {
		return drawList.get(drawList.size()-Useful.ONE).getDate();
	}	
//-----------------------------------------------------------------------	
	public boolean existCandidate(int candidate[]) {
		for(int i=Useful.ZERO;i<drawList.size();i++) {
			if(Useful.areEqualArrays(drawList.get(i).getDrawArray(),candidate) == true) {
				return true;
			}
		}
		return false;
	}	
//-----------------------------------------------------------------------	
	public int countList() {
		return drawList.size();
	}
//-----------------------------------------------------------------------							
	public int[] getLastArray() {
		return drawList.get(drawList.size()-Useful.ONE).getDrawArray();
	}

}//end class