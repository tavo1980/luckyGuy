package collection;
import useful.*;

public class Draw {
	
	private int[] drawArray;
	private int number;
	private String date;

	public Draw() {
		drawArray = new int[Useful.FOURTEEN];
	}
	
	public void createDrawArray(String fields[]) {
		number = Useful.convertStringToInt(fields[Useful.ZERO]);
		date = fields[Useful.ONE];
		int index = Useful.ZERO;
		for(int i=Useful.TWO;i<=Useful.FIVETEEN;i++) {
			drawArray[index] = Useful.convertStringToInt(fields[i]);
			index++;
		}		
	}

	public int[] getDrawArray() {
		return drawArray;
	}

	public int getNumber() {
		return number;
	}

	public String getDate() {
		return date;
	}
	
}//end class