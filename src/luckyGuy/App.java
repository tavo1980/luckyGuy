package luckyGuy;
import info.*;
import collection.*;
import useful.*;

public class App {
	
	private Info info;
	private DrawCollection drawCollection;
	private int[] candidate;
	private int values[];
	private int last;
	private String lastDate;
	
	public App() {
		info = new Info();
		drawCollection = new DrawCollection();
		candidate = new int[Useful.FOURTEEN];
		values = new int[Useful.ELEVEN];
	}
	
	public void getInfo() {
		info.getInfo();
	}
	
	public void getValues() {
		drawCollection.createDrawList();
		drawCollection.getFrecuency(values);
		drawCollection.getTermination(values);
		drawCollection.getDigit(values);
		drawCollection.getPrevius(values);
		drawCollection.getConsecutive(values);
		drawCollection.getSeparated(values);
		drawCollection.getMaxMin(values);
		drawCollection.getPair(values);
		drawCollection.getPrime(values);		
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(values);
	}
	/*
	*  posici�n 0 = frecuencia
	*  posici�n 1 = terminaci�n 2
	*  posici�n 2 = terminaci�n 3
	*  posici�n 3 = digitos
	*  posici�n 4 = previo
	*  posici�n 5 = consecutivo
	*  posici�n 6 = separaci�n
	*  posici�n 7 = m�x
	*  posici�n 8 = m�n
	*  posici�n 9 = par
	*  posici�n 10 = primo
	*/
//-----------------------------------------------------------------------		
	public void getLast() {
		last = getNumber();	
		lastDate = getDate();
		System.out.println("�ltimo:\t\t"+last);
		System.out.println("Fecha:\t\t"+lastDate);	
	}
//-----------------------------------------------------------------------
	public void evaluateCandidateAndValues() {
		System.out.println("...........................................");
		boolean flag = true;
		int countValues = Useful.ZERO;
		createCandidate();
		Useful.sortArray(candidate);
		while(flag && countValues < values.length) {
			//-----------------------------------------------
			if(existCandidate() == false) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			if(evaluateFrecuency() == true) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			if(evaluateTerminationTwo() == true) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			if(evaluateTerminationThree() == true) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			if(evaluateDigit() == true) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			if(evaluatePrevius() == true) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			if(evaluateConsecutive() == true) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			if(evaluateSeparated() == true) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			if(evaluateMaxMin() == true) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			if(evaluatePair() == true) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			if(evaluatePrime() == true) {
				countValues++;
			}else {
				evaluateCandidateAndValues();
			}
			//----------------------------------------------------
			break;
		}
		/*
		 * ver coleccion 
		 */
		//Useful.viewArray(candidate);	
	}
//-----------------------------------------------------------------------
	public void getCandidate() {
		Useful.viewCandidateInRows(candidate);
	}
//-----------------------------------------------------------------------
	public boolean existCandidate() {
		return drawCollection.existCandidate(candidate);
	}
	
	public void createCandidate() {
		Useful.initializateArray(candidate);
		int position = Useful.ONE;
		candidate[Useful.ZERO] = Useful.getRandom();
		do {
			int number = Useful.getRandom();
			while(Useful.existNumberInArray(number, candidate)) {
				number = Useful.getRandom();
			}
			candidate[position] = number;
			position++;
		}while(position < candidate.length);
	}	
//-----------------------------------------------------------------------	
	public boolean evaluateFrecuency() {
		if(Useful.countArray(candidate) == values[Useful.ZERO]) {
			return true;
		}
		return false;			
	}
//-----------------------------------------------------------------------		
	public boolean evaluateTerminationTwo() {
		if(Useful.countTermination(candidate,Useful.TWO) == values[Useful.ONE]) {
			return true;
		}
		return true;	
	}
	
	public boolean evaluateTerminationThree() {
		if(Useful.countTermination(candidate,Useful.THREE) == values[Useful.TWO]) {
			return true;
		}
		return true;	
	}	
//-----------------------------------------------------------------------	
	public boolean evaluateDigit() {
		if(Useful.countDigit(candidate) == values[Useful.THREE]) {
			return true;
		}
		return true;
	}	
//-----------------------------------------------------------------------	
	public boolean evaluatePrevius() {
		if(Useful.comparePrevius(drawCollection.getLastArray(),candidate) == values[Useful.FOUR]) {
			return true;
		}
		return true;	
	}	
//-----------------------------------------------------------------------		
	public boolean evaluateConsecutive() {
		if(Useful.countConsecutives(candidate) == values[Useful.FIVE]) {
			return true;
		}
		return true;	
	}	
//-----------------------------------------------------------------------		
	public boolean evaluateSeparated() {
		if(Useful.countSeparated(candidate) == values[Useful.SIX]) {
			return true;
		}
		return true;	
	}	
//-----------------------------------------------------------------------		
	public boolean evaluateMaxMin() {
		int sum = Useful.sumDraw(candidate);
		if(sum <= values[Useful.SEVEN] && sum >= values[Useful.EIGHT]) {
			return true;
		}
		return true;
	}	
//-----------------------------------------------------------------------
	public boolean evaluatePair() {
		if(Useful.countPair(candidate) == values[Useful.NINE]) {
			return true;
		}
		return true;
	}	
//-----------------------------------------------------------------------	
	public boolean evaluatePrime() {
		if(Useful.countPrime(candidate) == values[Useful.NINE]) {
			return true;
		}
		return true;
	}	
//-----------------------------------------------------------------------	
	public int getNumber() {
		return drawCollection.getNumber();
	}
//-----------------------------------------------------------------------	
	public String getDate() {
		return drawCollection.getDate();
	}

	public void viewLine() {
		System.out.println("=============================================");
	}
	
	public void title() {
		System.out.println("> Evaluando resultados.....................");
	}
	
}//end class