package info;
import java.util.*;

public class Info {
	
	private String name;
	private String author;
	private String version;
	
	public Info() {
		name = "LuckyGuy";
		author = "Octavio Oyanedel Alarc�n";
		version = "4.0";
	}
	
	public void getInfo() {
		System.out.println("Fecha:\t\t"+getDate());
		System.out.println("App:\t\t"+name);
		System.out.println("Version:\t"+version);
		System.out.println("Autor:\t\t"+author);
	}
	
	public Date getDate() {
		Date dateSystem = new Date();
		return dateSystem;
	}

}//end class